package com.example.appmenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenu.database.Alumno
import com.example.appmenu.database.dbAlumnos
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ExitFragment : Fragment() {

    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var db: dbAlumnos
    private lateinit var searchView: SearchView
    private lateinit var listaAlumnos: ArrayList<Alumno>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_exit, container, false)

        rcvLista = view.findViewById(R.id.recId)
        searchView = view.findViewById(R.id.searchView)
        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        db = dbAlumnos(requireContext())
        db.openDataBase()

        listaAlumnos = db.leerTodos()
        adaptador = MiAdaptador(listaAlumnos, requireContext())
        rcvLista.adapter = adaptador

        db.close()

        setupSearchView()

        view.findViewById<FloatingActionButton>(R.id.agregarAlumno).setOnClickListener {
            val fragment = DbFragment()
            val activity = context as AppCompatActivity
            val currentFragment = activity.supportFragmentManager.findFragmentById(R.id.ac)

            activity.supportFragmentManager.beginTransaction().apply {
                currentFragment?.let {
                    if (it.isVisible) {
                        hide(it)
                    }
                }
                replace(R.id.ac, fragment)
                addToBackStack(null)
                commit()
            }

            (requireActivity() as AppCompatActivity).findViewById<BottomNavigationView>(R.id.btnNavegador)
                .menu.findItem(R.id.btnDb).isChecked = true

            view.findViewById<FloatingActionButton>(R.id.agregarAlumno).hide()
        }

        return view
    }

    private fun setupSearchView() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    filterAlumno(it)
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    filterAlumno(it)
                }
                return true
            }
        })
    }

    private fun filterAlumno(query: String) {
        val filteredList = listaAlumnos.filter {
            it.nombre.contains(query, ignoreCase = true) ||
                    it.matricula.contains(query, ignoreCase = true)
        }
        adaptador.updateList(ArrayList(filteredList))
    }
}

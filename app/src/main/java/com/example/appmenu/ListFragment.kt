package com.example.appmenu

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView

class ListFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var searchView: SearchView
    private lateinit var textViewListaAlumnos: TextView
    private lateinit var originalItems: List<String>
    private lateinit var filteredItems: MutableList<String>
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        listView = view.findViewById(R.id.lstAlumnos)
        searchView = view.findViewById(R.id.menu_search)
        textViewListaAlumnos = view.findViewById(R.id.tvListaAlumnos)
        val items = resources.getStringArray(R.array.alumnos)

        originalItems = items.toList()
        filteredItems = originalItems.toMutableList()
        adapter = ArrayAdapter(requireContext(), R.layout.custom_list_item, filteredItems)

        listView.adapter = adapter
        listView.setOnItemClickListener { parent, view, position, id ->
            val alumno = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setMessage("$position: $alumno")
            builder.setPositiveButton("Ok") { dialog, which -> }
            builder.show()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                filterList(newText)
                return true
            }
        })

        searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                textViewListaAlumnos.visibility = View.GONE
            } else {
                textViewListaAlumnos.visibility = View.VISIBLE
            }
        }

        val searchText = searchView.findViewById<TextView>(androidx.appcompat.R.id.search_src_text)
        searchText.setTextColor(Color.BLACK)
        searchText.setHintTextColor(Color.BLACK)

        return view
    }

    private fun filterList(query: String?) {
        filteredItems.clear()
        if (query.isNullOrEmpty()) {
            filteredItems.addAll(originalItems)
        } else {
            val trimmedQuery = query.trim()
            for (item in originalItems) {
                if (item.contains(trimmedQuery, ignoreCase = true)) {
                    filteredItems.add(item)
                }
            }
        }
        adapter.notifyDataSetChanged()
    }
}

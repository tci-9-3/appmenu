package com.example.appmenu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class HomeFragment : Fragment() {
    private lateinit var imgAlumno: ImageView
    private lateinit var txtNombre: TextView
    private lateinit var txtMateria: TextView
    private lateinit var txtCarrera: TextView
    private lateinit var txtCorreo: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        imgAlumno = view.findViewById(R.id.imgAlumno)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtMateria = view.findViewById(R.id.txtMateria)
        txtCarrera = view.findViewById(R.id.txtCarrera)
        txtCorreo = view.findViewById(R.id.txtCorreo)

        txtNombre.text = "Mario Leinad Arredondo Fraide"
        txtMateria.text = "Programación Móvil"
        txtCarrera.text = "Ingeniería en Tecnologías de la Información"
        txtCorreo.text = "mario_leinad02@hotmail.com"

        return view
    }
}
package com.example.appmenu

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.appmenu.database.Alumno
import com.example.appmenu.database.dbAlumnos
import com.google.android.material.bottomnavigation.BottomNavigationView

class DbFragment : Fragment() {
    private lateinit var btnGuardar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var imgFoto: ImageView
    private lateinit var db: dbAlumnos


    companion object {
        private const val PICK_IMAGE_REQUEST = 1
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        imgFoto = view.findViewById(R.id.imgFoto)

        view.findViewById<Button>(R.id.btnSeleccionarFoto).setOnClickListener {
            seleccionarFoto()
        }

        imgFoto.setImageResource(R.drawable.profile)

        arguments?.getParcelable<Alumno>("alumno")?.let { alumno ->

            txtMatricula.setText(alumno.matricula)
            txtNombre.setText(alumno.nombre)
            txtDomicilio.setText(alumno.domicilio)
            txtEspecialidad.setText(alumno.especialidad)

            if (alumno.foto.isNotEmpty()) {
                Glide.with(requireContext())
                    .load(Uri.parse(alumno.foto))
                    .apply(RequestOptions().override(100, 100))
                    .into(imgFoto)
                imgFoto.tag = alumno.foto
            } else {

                imgFoto.setImageResource(R.drawable.profile)
                imgFoto.tag = null
            }

            (requireActivity() as AppCompatActivity).findViewById<BottomNavigationView>(R.id.btnNavegador)
                .menu.findItem(R.id.btnDb).isChecked = true
        }


        btnGuardar.setOnClickListener {
            val nombre = txtNombre.text.toString()
            val matricula = txtMatricula.text.toString()
            val domicilio = txtDomicilio.text.toString()
            val especialidad = txtEspecialidad.text.toString()
            val fotoUrl = imgFoto.tag?.toString() ?: ""

            if (nombre.isEmpty() || matricula.isEmpty() || domicilio.isEmpty() || especialidad.isEmpty() || fotoUrl.isEmpty()) {
                Toast.makeText(
                    requireContext(), "Campos Faltantes", Toast.LENGTH_SHORT
                ).show()
            } else {
                db = dbAlumnos(requireContext())
                db.openDataBase()

                // Verificar si el alumno ya existe por matrícula
                val existingAlumno = db.getAlumno(matricula)

                if (existingAlumno.id != 0) {
                    // Actualizar el alumno existente
                    existingAlumno.nombre = nombre
                    existingAlumno.domicilio = domicilio
                    existingAlumno.especialidad = especialidad
                    existingAlumno.foto = fotoUrl

                    val rowsAffected = db.actualizarAlumno(existingAlumno, existingAlumno.id)
                    if (rowsAffected > 0) {
                        Toast.makeText(
                            requireContext(),
                            "Alumno actualizado exitosamente", Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Error al actualizar alumno", Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    // Si no existe, insertar un nuevo alumno
                    val nuevoAlumno = Alumno().apply {
                        this.nombre = nombre
                        this.matricula = matricula
                        this.domicilio = domicilio
                        this.especialidad = especialidad
                        this.foto = fotoUrl
                    }

                    val id: Long = db.insertarAlumno(nuevoAlumno)
                    if (id != -1L) {
                        Toast.makeText(
                            requireContext(),
                            "Registro Exitoso con ID $id", Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Error al insertar alumno", Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                db.close()

                // Limpiar los campos después de guardar o actualizar
                txtNombre.setText("")
                txtMatricula.setText("")
                txtDomicilio.setText("")
                txtEspecialidad.setText("")
                imgFoto.setImageResource(R.drawable.profile)
                imgFoto.tag = null
            }
        }

        btnBuscar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Matricula sin capturar", Toast.LENGTH_SHORT).show()
            } else {
                db = dbAlumnos(requireContext())
                db.openDataBase()

                val alumno: Alumno = db.getAlumno(txtMatricula.text.toString())
                if (alumno.id != 0) {
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    // Mostrar la imagen si está disponible
                    Glide.with(requireContext())
                        .load(Uri.parse(alumno.foto)) // Esto asume que alumno.foto es una URL o ruta válida
                        .apply(RequestOptions().override(100, 100)) // Opciones adicionales como tamaño
                        .into(imgFoto)
                    imgFoto.tag = alumno.foto
                } else {
                    Toast.makeText(requireContext(), "Matricula no encontrada", Toast.LENGTH_SHORT).show()
                }

                db.close()
            }
        }

        btnBorrar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Matricula sin capturar", Toast.LENGTH_SHORT).show()
            } else {
                // Crear un AlertDialog para confirmar la eliminación
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle("Confirmar eliminación")
                builder.setMessage("¿Estás seguro de que deseas eliminar este alumno?")

                // Agregar botón de confirmación
                builder.setPositiveButton("Sí") { dialog, which ->
                    db = dbAlumnos(requireContext())
                    db.openDataBase()

                    val alumno: Alumno = db.getAlumno(txtMatricula.text.toString())
                    if (alumno.id != 0) {
                        val result = db.borrarAlumno(alumno.id)
                        if (result > 0) {
                            Toast.makeText(requireContext(), "Alumno eliminado exitosamente", Toast.LENGTH_SHORT).show()
                            txtNombre.setText("")
                            txtDomicilio.setText("")
                            txtEspecialidad.setText("")
                            txtMatricula.setText("")
                            imgFoto.setImageResource(R.drawable.profile)
                            imgFoto.tag = null
                        } else {
                            Toast.makeText(requireContext(), "Error al eliminar alumno", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(requireContext(), "Matricula no encontrada", Toast.LENGTH_SHORT).show()
                    }

                    db.close()
                }

                // Agregar botón de cancelación
                builder.setNegativeButton("No") { dialog, which ->
                    dialog.dismiss()
                }

                // Mostrar el AlertDialog
                val dialog = builder.create()
                dialog.show()
            }
        }

        // Configurar el botón para seleccionar una foto
        imgFoto.setOnClickListener {
            seleccionarFoto()
        }

        return view
    }

    // Método para seleccionar una foto de la galería
    private fun seleccionarFoto() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_IMAGE_REQUEST)
    }

    // Manejo del resultado de la selección de imagen
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            val uri: Uri? = data.data
            imgFoto.setImageURI(uri)
            imgFoto.tag = uri.toString() // Guardar la URL de la imagen en el tag de ImageView
        }
    }
}
